function hoverimg(id, style){
  $(id).attr('style', style)
}

function register(form) {
  var postData = $(form).serializeArray();
  var formURL = $(form).attr("action");
  var validate = 0;
  $('#status_response').removeClass('d-none');
  $('form' + form + ' .form-control[required]').each(function () {
    if (!$(this).val()) {
      alertmsg('O campo ' + $(this).attr('title') + ' é obrigatório!', 'alert-danger');
      $(this).addClass('is-invalid');
      $('#status_response').addClass('d-none');
      validate++;
      return false;
    }
  });

  if (validate == 0) {
    $.ajax({
      type: "POST",
      url: formURL,
      data: postData,
      dataType: 'json',
      success: function (data) {
        var response = data['response'];
        alertmsg(response['mensagem'], response['classe'])

        if (response['result'] == 'error') {
          $('.validate-input').each(function () {
            if ($(this).val() == '') {
              $(this).addClass('is-invalid');
            }
          });
          return false;
        } else {
          window.location.replace(response['redirect']);
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
        return false;
      }
    });
  }

}

function alertmsg(msg, classe) {
  $('#status_response').addClass('d-none');
  $('#response').html('<div class="alert-dismissible fade show p-2" role="alert">' + msg + '</div>');
  $('#response').removeClass('alert alert-danger');
  $('#response').removeClass('alert alert-success');
  $('#response').addClass('alert ' + classe);
  $("#response").slideDown();
  setTimeout(function () {
    $("#response").slideUp();
  }, 4000);
}

$('#owl-slide').owlCarousel({
  loop: true,
  margin: 30,
  responsiveClass: true,
  autoplay: false,
  autoplayTimeout: 4000,
  autoplayHoverPause: true,
  center: true,
  dots: false,
  responsive: {
    0: {
      items: 1,
      nav: false
    },
    600: {
      items: 1,
      nav: false
    },
    1000: {
      items: 1,
      nav: true,
      navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]
    }
  }
});

