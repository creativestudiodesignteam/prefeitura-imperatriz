<!DOCTYPE html>
<html lang="en">
<head>
    <title>Passo 03</title>
    <?php include('includes/head.php') ?>
</head>
<body>

    <section class="container mt-4" id="header">
        <nav class="mb-4">
            <div class="row">
                <div class="col-lg-6 text-sm-center text-md-left">
                    <a class="navbar-brand" href="index.php"><img src="assets/images/logo.png" alt=""></a>
                </div>
                <div class="col-lg-6 float-right">
                    
                </div>
            </div>
        </nav>
        <div class="row step-03">
            <div class="col-lg-12">
                <p class="text-center">
                    Passo 3 de 4
                </p>
                <p class="text-center">
                    <span class="bg-green steps-button">Passo 03</span>
                </p>

                <div class="row" id="step">
                    <div class="col-lg-6 offset-lg-3 text-left box">
                        <h1 class="title mb-0">Verifique se recebeu o SMS com o código</h1>
                        <p class="text-left-center">Código de 4 dígitos enviado para +55 11 95959-6659</p>
                        <form action="step4.php">
                            <div class="label-float">
                                <input type="text" placeholder=" "/>
                                <label>Insira o código aqui</label>
                            </div>
                            <div class="text-left-center mt-3">
                                <button class="btn mr-lg-4 btn-green text-white">Conectar ao wi-fi</button>
                                <a class="link align-link-modal" href="#.">Tentar de outro jeito</a>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-6 offset-lg-3 mt-4">
                        <p class="text-center">
                            Seu <span class="bold">ISP</span> e a <span class="bold">OnCABO</span>. 
                            Ao se conectar, você concorda com os Termos de Serviço da <span class="bold underline" data-toggle="modal" data-target="#prefeitura" style="cursor: pointer">Prefeitura</span> e da <span class="bold underline" data-toggle="modal" data-target="#oncabo" style="cursor: pointer">OnCABO</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'includes/footer.php'?>
    </section>    
<?php include 'includes/scripts.php'?>
</body>
</html>