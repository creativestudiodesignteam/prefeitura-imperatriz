<!DOCTYPE html>
<html lang="en">
<head>
    <title>Conectado</title>
    <?php include('includes/head.php') ?>
</head>
<body>

    <section class="container mt-4" id="header">
        <nav class="mb-4">
            <div class="row">
                <div class="col-lg-6 text-sm-center text-md-left">
                    <a class="navbar-brand" href="index.php"><img src="assets/images/logo.png" alt=""></a>
                </div>
                <div class="col-lg-6 float-right">
                    
                </div>
            </div>
        </nav>
        <div class="row step-05">
            <div class="col-lg-12">
                <div class="row" id="step">
                    <div class="col-lg-6 offset-lg-3 text-center box">
                        <h1 class="title">Você está conectado</h1>
                        <p>Você será redirecionado para prefeituraimperatriz.org...</p>
                        <p><span id="timer" class="bg-green  steps-button"><i class="far fa-clock"></i> <?php echo date('H:i') ?></span></p>
                    </div>
                    <div class="col-lg-6 offset-lg-3 mt-4">
                        <p class="text-center">
                            Seu <span class="bold">ISP</span> e a <span class="bold">OnCABO</span>. 
                            Ao se conectar, você concorda com os Termos de Serviço da <span class="bold underline" data-toggle="modal" data-target="#prefeitura" style="cursor: pointer">Prefeitura</span> e da <span class="bold underline" data-toggle="modal" data-target="#oncabo" style="cursor: pointer">OnCABO</span>
                        </p>
                    </div>
                </div>
                
            </div>
        </div>
        <?php include 'includes/footer.php'?>
    </section>    
<?php include 'includes/scripts.php'?>
</body>
</html>