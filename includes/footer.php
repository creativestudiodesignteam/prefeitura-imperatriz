<div class="row mb-5 mt-5 footer">
    <div class="col-md-6">
        <p class="text-sm-center dev">Desenvolvido por | <a href="https://www.creativedd.com.br">Creative Dev & Design</a></p>
    </div>
    <div class="col-md-6">
        <p class="text-align-links-footer">
            <a class="mr-3"  data-toggle="modal" data-target="#sobre" href="#sobre">Sobre o serviço</a>
            <a class="mr-3"  data-toggle="modal" data-target="#privacidade" href="#.">Privacidade</a>
            <a href="#."  data-toggle="modal" data-target="#termos">Termos</a>
        </p>
    </div>
</div>

<!-- Sobre -->
<div class="modal fade" id="sobre" tabindex="-1" role="dialog" aria-labelledby="sobre" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="sobreLabel">Sobre</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perspiciatis harum quas, in eos sapiente sit blanditiis magnam magni dolorem iste est voluptas placeat incidunt cumque doloribus fugit cupiditate qui. Deserunt!
      </div>
    </div>
  </div>
</div>

<!-- Privacidade -->
<div class="modal fade" id="privacidade" tabindex="-1" role="dialog" aria-labelledby="privacidade" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="privacidadeLabel">Privacidade</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perspiciatis harum quas, in eos sapiente sit blanditiis magnam magni dolorem iste est voluptas placeat incidunt cumque doloribus fugit cupiditate qui. Deserunt!
      </div>
    </div>
  </div>
</div>

<!-- Prefeitura -->
<div class="modal fade" id="prefeitura" tabindex="-1" role="dialog" aria-labelledby="prefeitura" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="termosLabel">Prefeitura</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perspiciatis harum quas, in eos sapiente sit blanditiis magnam magni dolorem iste est voluptas placeat incidunt cumque doloribus fugit cupiditate qui. Deserunt!
      </div>
    </div>
  </div>
</div>

<!-- OnCABO -->
<div class="modal fade" id="oncabo" tabindex="-1" role="dialog" aria-labelledby="oncabo" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="oncaboLabel">OnCABO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perspiciatis harum quas, in eos sapiente sit blanditiis magnam magni dolorem iste est voluptas placeat incidunt cumque doloribus fugit cupiditate qui. Deserunt!
      </div>
    </div>
  </div>
</div>