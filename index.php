<!DOCTYPE html>
<html lang="en">
<head>
    <title>Passo 01</title>
    <?php include('includes/head.php') ?>
</head>
<body>
    
    <section class="container mt-4" id="header">
        <div class="row">
            <div class="col-lg-8">
                <nav class="mb-4">
                    <div class="row">
                        <div class="col-lg-6 text-sm-center text-md-left">
                            <a class="navbar-brand" href="index.php"><img src="assets/images/logo.png" alt=""></a>
                        </div>
                        <div class="col-lg-6 float-right">
                            <p class="align-step mmt-20">
                                Passo 1 de 4
                            </p>
                        </div>
                    </div>
                </nav>
                <!-- <div class="bg-green btn-center-topzera">Passo 01</div> -->

                <div class="row">
                    <div class="col-lg-8">
                        <h1 class="title title-index mt-3 aling-aux">Cidade conectada!</h1>
                        <p class="text-sm-center text-md-left">Internet gratuita para locais públicos de população carente</p>
                    </div>
                    <div class="col-lg-4 align-self-center text-right-center">
                        <a href="step2.php" class="btn btn-green text-white  steps-button">Iniciar conexão</a>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <div class="icon-border">
                                    <img src="assets/images/wifi.png" alt="">
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <h3 class="title">Rápida</h3>
                                <p class="text-sm-center text-md-left">Você terá a mesma experiência em todos os lugares.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mt-2">
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <div class="icon-border">
                                    <img src="assets/images/shield.png" alt="">
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <h3 class="title">Segura</h3>
                                <p class="text-sm-center text-md-left">Conexão segura para todos os usuários.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-lg-6 mt-2">
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <div class="icon-border">
                                    <img src="assets/images/radio.png" alt="">
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <h3 class="title">Estátal</h3>
                                <p class="text-sm-center text-md-left">Uma conexão instável para todos usarem com a melhor qualidade.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mt-2">
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <div class="icon-border">
                                    <img src="assets/images/high-five.png" alt="">
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <h3 class="title">Para todos</h3>
                                <p class="text-sm-center text-md-left">Lemos wi-fi para vários lugares confirme no mapa abaixo.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-lg-12">
                        <h1 class="title">Nossas localidades</h1>
                        <p class="text-sm-center text-md-left">Conheça todos os espaços públicos de wi-fi livre</p>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-md-12">
                    <div id="map" style="height: 500px;"></div>
                    </div>
                </div>
                <?php include 'includes/footer.php'?>
            </div>
            <div class="col-lg-4 general p-0 d-none d-md-block">
                <div id="owl-slide" class="owl-carousel owl-theme">
                    <div class="item">
                        <img src="assets/images/slide-01.jpg">
                    </div>
                    <div class="item">
                        <img src="assets/images/slide-02.jpg">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include 'includes/scripts.php'?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAb1zy026nCnsbnexkvbO9MlSiSVntqUWs&callback=initMap" async defer></script>
    <script>
        function initMap() {

            var myLatLng = {lat: -21.277034, lng: -50.33185};

            var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: myLatLng
            });

        }
    </script>
</body>
</html>