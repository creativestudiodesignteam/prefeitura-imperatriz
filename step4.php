<!DOCTYPE html>
<html lang="en">
<head>
    <title>Passo 04</title>
    <?php include('includes/head.php') ?>
</head>
<body>

    <section class="container mt-4" id="header">
        <nav class="mb-4">
            <div class="row">
                <div class="col-lg-6 text-sm-center text-md-left">
                    <a class="navbar-brand" href="index.php"><img src="assets/images/logo.png" alt=""></a>
                </div>
                <div class="col-lg-6 float-right">
                    
                </div>
            </div>
        </nav>
        <div class="row step-04">
            <div class="col-lg-12">
                <p class="text-center">
                    Passo 4 de 4
                </p>
                <p class="text-center">
                    <span class="bg-green steps-button">Passo 04</span>
                </p>

                <div class="row" id="step">
                    <div class="col-lg-6 offset-lg-3 text-center">
                        <h1 class="title">Conectando...</h1>
                        <p><span id="timer" class="bg-green">08 segundos restantes</span></p>
                    </div>
                    <div class="col-lg-6 offset-lg-3 mt-4">
                        <iframe style="border-radius: 20px;" width="100%" height="315" src="https://www.youtube-nocookie.com/embed/0zSfP5_PxF0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                
            </div>
        </div>
        <?php include 'includes/footer.php'?>
    </section>    
<?php include 'includes/scripts.php'?>
<script>
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = seconds + ' segundos restantes';

        if (--timer < 0) {
            timer = duration;
        }

        if(seconds == 00){
            window.location.href = "step5.php";
        }
    }, 1000);
}

window.onload = function () {
    var time = 7,
        display = document.querySelector('#timer');
    startTimer(time, display);
};
</script>
</body>
</html>